FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive

RUN apt update
RUN apt install -y python3
RUN apt install -y nodejs
RUN apt install -y npm
RUN apt install -y python-is-python3
RUN apt install -y python3-venv
RUN apt install -y python3-pip
RUN apt install -y git

RUN apt install -y default-jre-headless

RUN apt clean && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

RUN rm /tmp/requirements.txt
RUN npm install -g rollup

RUN pip install pytest