from pyscreeps import *

def main():
    init() # Does some initial setup for using pyscreeps

    print(Game.rooms.keys())

    for room in Game.rooms.keys():
        room = Game.rooms[room]
        print(room.find(FIND_SOURCES)) # prints sources in your room

module.exports.loop = main