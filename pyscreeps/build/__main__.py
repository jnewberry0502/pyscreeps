import subprocess
import requests
import base64
import json
import argparse

def build():
    if subprocess.call("python -m transcrypt main.py -n -p .none -b".split()):
        raise Exception("Transcrypt failed, check the logs above to debug.")
    if subprocess.call("npx rollup --input __target__/main.js --file main.js --format cjs".split()):
        raise Exception("Rollup failed, check the logs above to debug.")

def upload(config):
    module_files = {
        "main": open("main.js").read()
    }

    post_url = f'{config["url"]}/api/user/code'

    post_data = json.dumps({'modules': module_files, 'branch': config.get("branch", "default")})

    headers = {
        'Content-Type': 'application/json; charset=utf-8'
    }

    if config.get("token", None) is not None:
        headers['X-Token'] = config["token"]
    else:
        auth_pair = f"{config['username']}:{config['password']}"
        headers['Authorization'] = 'Basic ' + base64.b64encode(auth_pair.encode("utf-8")).decode("utf-8")

    response = requests.post(post_url, post_data, headers=headers)
    
    if not response.ok:
        raise Exception(response.content)
    
    data = response.json()

    print(f"uploading files to {config['url']}, branch {config['branch']}")

    if not data.get('ok'):
        raise Exception(data)

    print("upload successful.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", default="config.json")
    parser.add_argument("--no-build", dest="build", action='store_false')
    parser.add_argument("--no-upload", dest="upload", action='store_false')

    args = parser.parse_args()

    config = {
        "branch": "default",
        "url": "https://screeps.com"
    }
    
    config.update(json.load(open(args.config)))

    if args.build:
        build()
    
    if args.upload:
        upload(config)
