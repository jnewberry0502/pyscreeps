from typing import Optional, Type, Union, Dict, List

from .memory import _Memory

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .room import Room, RoomPosition
    from .structures import Structure
    from .creep import Creep

from dataclasses import dataclass, field

@dataclass(init=False, repr=False)
class _Effect:
    effect: int
    level: int
    ticksRemaining: int

Effects = List[_Effect]

@dataclass(init=False, repr=False)
class RoomObject:
    effects: Effects
    pos: 'RoomPosition'
    room: 'Room'


@dataclass(init=False, repr=False)
class Flag(RoomObject):
    effects: Effects
    color: int
    memory: _Memory
    name: str
    secondaryColor: int

    def remove(self) -> int:
        pass

    def setColor(self, color: int, secondaryColor: int = None) -> int:
        pass

    def setPosition(self, x: Union[int, 'RoomPosition', 'RoomObject'], y: int = None) -> int:
        pass

    @property
    def hint(self) -> int:
        return 0


@dataclass(init=False, repr=False)
class Source(RoomObject):
    energy: int
    energyCapacity: int
    id: str
    ticksToRegeneration: int


@dataclass(init=False, repr=False)
class Mineral(RoomObject):
    density: int
    mineralAmount: int
    mineralType: str
    id: str
    ticksToRegeneration: int


@dataclass(init=False, repr=False)
class Resource(RoomObject):
    amount: int
    id: str
    resourceType: str


@dataclass(init=False, repr=False)
class Store:
    def getCapacity(self, resource: str = None) -> Union[Dict[str, int], int]:
        pass

    def getFreeCapacity(self, resource: str = None) -> Union[Dict[str, int], int]:
        pass

    def getUsedCapacity(self, resource: str = None) -> Union[Dict[str, int], int]:
        pass


@dataclass(init=False, repr=False)
class Ruin(RoomObject):
    destroyTime: int
    id: str
    store: Dict[str, int]
    _Structure: 'Structure'
    ticksToDecay: int


@dataclass(init=False, repr=False)
class Tombstone(RoomObject):
    deathTime: int
    id: str
    store: Dict[str, int]
    _Structure: 'Structure'
    ticksToDecay: int