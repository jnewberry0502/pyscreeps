from dataclasses import dataclass
from typing import Any, Dict, List, Optional, Union

__all__ = ['Memory', 'RawMemory', '_Memory', '_MemoryValue']

_MemoryValue = Union[str, int, float, bool, '_Memory', List['_MemoryValue'], None]


class _Memory(dict):
    def __getattr__(self, key: str) -> _MemoryValue:
        pass

    def __setattr__(self, key: str, value: _MemoryValue) -> None:
        pass


Memory = _Memory()

@dataclass
class _ForeignSegment:
    username: str
    _id: int
    date: str

@dataclass
class RawMemory:
    segments: Dict[int, str]
    foreignSegment: Optional[_ForeignSegment]

    @classmethod
    def get(cls) -> str:
        pass

    @classmethod
    def set(cls, value: str) -> None:
        pass

    @classmethod
    def setActiveSegments(cls, ids: List[int]) -> None:
        pass

    @classmethod
    def setActiveForeignSegment(cls, username: Optional[str], _id: Optional[int] = None) -> None:
        pass

    @classmethod
    def setDefaultPublicSegment(cls, _id: Optional[int]) -> None:
        pass

    @classmethod
    def setPublicSegments(cls, ids: List[int]) -> None:
        pass
