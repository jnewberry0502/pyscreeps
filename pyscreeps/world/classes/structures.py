from __future__ import annotations
from dataclasses import dataclass, field

from typing import Any, Dict, List, Optional, Union

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .creep import Creep
    from .memory import _Memory
    from .room import Room, RoomPosition, _Owner

from .misc_obj import Effects, RoomObject, Store, _Effect


@dataclass(init=False, repr=False)
class Structure(RoomObject):
    id: str
    structureType: str
    hits: int
    hitsMax: int

    def destroy(self) -> int:
        pass

    def isActive(self) -> bool:
        pass

    def notifyWhenAttacked(self, enabled: bool) -> int:
        pass


@dataclass(init=False, repr=False)
class OwnedStructure(Structure):
    my: bool
    owner: _Owner


@dataclass(init=False, repr=False)
class ConstructionSite(RoomObject):
    effects: _Effect
    id: str
    my: bool
    owner: _Owner
    progress: int
    progressTotal: int
    structureType: str

    def remove(self) -> int:
        pass


@dataclass(init=False, repr=False)
class StructureContainer(Structure):
    effects: _Effect
    store: Store
    ticksToDecay: int

@dataclass(init=False, repr=False)
class _RoomReservation:
    username: str
    ticksToEnd: int

@dataclass(init=False, repr=False)
class _ControllerSign:
    username: str
    text: str
    time: int
    datetime: Any


@dataclass(init=False, repr=False)
class StructureController(OwnedStructure):
    effects: Effects = field(default_factory=list)
    level: int = 0
    progress: int = 0
    progressTotal: int = 0
    reservation: _RoomReservation | None = None
    safeMode: int = 0
    safeModeAvailable: int = 0
    safeModeCooldown: int = 0
    sign: _ControllerSign | None = None
    ticksToDowngrade: int = 0
    upgradeBlocked: int = 0

    def activateSafeMode(self) -> int:
        pass

    def unclaim(self) -> int:
        pass


@dataclass(init=False, repr=False)
class StructureExtension(OwnedStructure):
    effects: _Effect
    store: Store()


@dataclass(init=False, repr=False)
class StructureExtractor(OwnedStructure):
    effects: _Effect
    cooldown: int

@dataclass(init=False, repr=False)
class StructureFactory(OwnedStructure):
    effects: _Effect
    cooldown: int
    level: int
    store: Store

    def produce(self, resourceType: str) -> int:
        pass

@dataclass(init=False, repr=False)
class StructureInvaderCore(OwnedStructure):
    effects: Effects
    level: int
    ticksToDeploy: int


@dataclass(init=False, repr=False)
class StructureKeeperLair(OwnedStructure):
    effects: _Effect
    ticksToSpawn: int


@dataclass(init=False, repr=False)
class StructureLab(OwnedStructure):
    effects: _Effect
    cooldown: int
    mineralType: Optional[str]
    store: Store

    def boostCreep(self, creep: Creep, bodyPartsCount: Optional[int] = None) -> int:
        pass

    def reverseReaction(self, lab1: 'StructureLab', lab2: 'StructureLab') -> int:
        pass

    def runReaction(self, lab1: 'StructureLab', lab2: 'StructureLab') -> int:
        pass

    def unboostCreep(self, creep: Creep) -> int:
        pass


@dataclass(init=False, repr=False)
class StructureLink(OwnedStructure):
    effects: _Effect
    cooldown: int
    store: Store

    def transferEnergy(self, target: 'StructureLink', amount: int = 0) -> int:
        pass


@dataclass
class StructureNuker(OwnedStructure):
    effects: _Effect
    store: Store

    def launchNuke(self, pos: RoomPosition) -> int:
        pass


@dataclass
class StructureObserver(OwnedStructure):
    effects: _Effect

    def observeRoom(self, roomName: str) -> int:
        pass


@dataclass
class StructurePowerBank(Structure):
    effects: _Effect
    power: int
    ticksToDecay: int


@dataclass
class StructurePowerSpawn(OwnedStructure):
    effects: _Effect
    energy: int
    energyCapacity: int
    power: int
    powerCapacity: int
    cooldown: int
    store: Store

    def processPower(self) -> int:
        pass

@dataclass
class _ShardPortalDestination:
    shard: str
    room: str


@dataclass
class StructurePortal(Structure):
    effects: _Effect
    destination: Union[RoomPosition, _ShardPortalDestination]
    ticksToDecay: Optional[int]


@dataclass
class StructureRampart(OwnedStructure):
    effects: _Effect
    isPublic: bool
    ticksToDecay: int

    def setPublic(self, isPublic: bool) -> int:
        pass


# noinspection PyPep8Naming
class StructureRoad(Structure):
    """
    :type effects: _Effect
    :type ticksToDecay: int
    """

    def __init__(self, effects: _Effect, pos: RoomPosition, room: Room,
                 structureType: str, _id: str, hits: int, hitsMax: int,
                 ticksToDecay: int) -> None:
        """
        WARNING: This constructor is purely for type completion, and does not exist in the game.
        """
        super().__init__(effects, pos, room, structureType, _id, hits, hitsMax)

        self.ticksToDecay = ticksToDecay


# noinspection PyPep8Naming
class _SpawnSpawningCreep:
    """
    :type directions: List[int]
    :type name: str
    :type needTime: int
    :type remainingTime: int
    :type spawn: StructureSpawn
    """

    def __init__(self, directions: List[int], name: str, needTime: int, remainingTime: int, spawn: str):
        self.directions = directions
        self.name = name
        self.needTime = needTime
        self.remainingTime = remainingTime
        self.spawn = spawn

    def cancel(self) -> int:
        pass

    def setDirections(self, directions: List[int]) -> int:
        pass


# noinspection PyPep8Naming
class StructureSpawn(OwnedStructure):
    """
    :type effects: _Effect
    :type memory: _Memory
    :type name: str
    :type spawning: Optional[_SpawnSpawningCreep]
    :type store: Store
    """

    def __init__(self, effects: _Effect, pos: RoomPosition, room: Room,
                 structureType: str, _id: str, hits: int, hitsMax: int,
                 my: bool, owner: _Owner,  # energy: int, energyCapacity: int,
                 memory: _Memory, name: str,
                 spawning: Optional[_SpawnSpawningCreep],
                 store: Store) -> None:
        """
        WARNING: This constructor is purely for type completion, and does not exist in the game.
        """
        super().__init__(effects, pos, room, structureType, _id, hits, hitsMax, my, owner)
        self.memory = memory
        self.name = name
        self.spawning = spawning
        self.store = store
    def canCreateCreep(self, body: List[str], name: Optional[str] = None) -> int:
        pass

    def createCreep(self, body: List[str], name: Optional[str] = None, memory: Optional[Dict[str, Any]] = None) \
            -> Union[int, str]:
        pass

    def spawnCreep(self, body: List[str], name: str, opts: Optional[Dict[str, Any]] = None) -> int:
        pass

    def recycleCreep(self, target: Creep) -> int:
        pass

    def renewCreep(self, target: Creep) -> int:
        pass


# noinspection PyPep8Naming
class StructureStorage(OwnedStructure):
    """
    :type effects: _Effect
    :type store: Store
    """

    def __init__(self, effects: _Effect, pos: RoomPosition, room: Room,
                 structureType: str, _id: str, hits: int, hitsMax: int,
                 my: bool, owner: _Owner, store: Store) -> None:
        """
        WARNING: This constructor is purely for type completion, and does not exist in the game.
        """
        super().__init__(effects, pos, room, structureType, _id, hits, hitsMax, my, owner)
        self.store = store


# noinspection PyPep8Naming
class StructureTerminal(OwnedStructure):
    """
    :type effects: _Effect
    :type cooldown: int
    :type store: dict[str, int]
    """

    def __init__(self, effects: _Effect, pos: RoomPosition, room: Room,
                 structureType: str, _id: str, hits: int, hitsMax: int,
                 my: bool, owner: _Owner, cooldown: int, store: Store) -> None:
        """
        WARNING: This constructor is purely for type completion, and does not exist in the game.
        """
        super().__init__(effects, pos, room, structureType, _id, hits, hitsMax, my, owner)
        self.cooldown = cooldown
        self.store = store

    def send(self, resourceType: str, amount: Union[int, float], destination: str, description: str = None) -> int:
        pass


# noinspection PyPep8Naming
class StructureTower(OwnedStructure):
    """
    :type effects: _Effect
    :type store: Store
    """

    def __init__(self, effects: _Effect, pos: RoomPosition, room: Room,
                 structureType: str, _id: str, hits: int, hitsMax: int,
                 my: bool, owner: _Owner, store: Store) -> None:
        """
        WARNING: This constructor is purely for type completion, and does not exist in the game.
        """
        super().__init__(effects, pos, room, structureType, _id, hits, hitsMax, my, owner)
        self.store = store

    def attack(self, target: Creep) -> int:
        pass

    def heal(self, target: Creep) -> int:
        pass

    def repair(self, target: Structure) -> int:
        pass


# noinspection PyPep8Naming
class StructureWall(Structure):
    """
    :type effects: _Effect
    """

    def __init__(self, effects: _Effect, pos: RoomPosition, room: Room,
                 structureType: str, _id: str, hits: int, hitsMax: int) -> None:
        """
        WARNING: This constructor is purely for type completion, and does not exist in the game.
        """
        super().__init__(effects, pos, room, structureType, _id, hits, hitsMax)
