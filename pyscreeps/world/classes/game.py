from dataclasses import dataclass
from typing import Any, Callable, Dict, List, Optional, Union

from pyscreeps.constants import PWR_OPERATE_OBSERVER
from .creep import Creep
from .misc_obj import Flag, RoomObject
from .room import Room, RoomPosition, _Owner
from .structures import ConstructionSite, OwnedStructure, Structure, StructureSpawn

@dataclass
class _GameCpu:
    limit: int
    tickLimit: int
    bucket: int
    shardLimits: Dict[str, int]

    def getUsed(self) -> float:
        pass

    def setShardLimits(self, shardLimits: Dict[str, int]) -> int:
        pass

@dataclass
class _GameShard:
    name: str
    type: str
    ptr: bool

@dataclass
class _GameGcl:
    level: int
    progress: int
    progressTotal: int

@dataclass
class _GameGpl:
    level: int
    progress: int
    progressTotal: int

@dataclass
class _GameMap:
    def describeExits(self, roomName: str) -> Dict[int, str]:
        pass

    def findExit(self, fromRoom: str, toRoom: str, opts: Dict[str, Any]) -> int:
        pass

    def findRoute(self, fromRoom: str, toRoom: str, opts: Dict[str, Any]) -> List[Dict[str, Union[int, str]]]:
        pass

    def getRoomLinearDistance(self, roomName1: str, roomName2: str, terminalDistance: bool = False) -> int:
        pass

    def getRoomTerrain(self, roomName: str) -> Room.Terrain:
        pass

    def getTerrainAt(self, x: Union[int, RoomPosition], y: int = None, roomName: str = None) -> str:
        pass

    def getWorldSize(self, roomName: str) -> int:
        pass

    def isRoomAvailable(self, roomName: str) -> bool:
        pass

@dataclass
class _MarketTransactionOrder:
    id: str
    type: str
    deprice: int

@dataclass
class _MarketTransaction:
    transactionId: str
    time: int
    sender: _Owner
    recipient: _Owner
    resourceType: str
    amount: int
    from_: str
    to: str
    description: str
    order: Optional[_MarketTransactionOrder]

@dataclass
class _MarketOrder:
    id: str
    created: int
    type: str
    resourceType: str
    roomName: str
    amount: int
    remainingAmount: int
    price: float 


@dataclass
class _MarketHistory:
    resourceType: str
    data: str
    transactions: int
    volume: int
    avgPrice: float
    stddevPrice: float


@dataclass
class _OwnedMarketOrder(_MarketOrder):
    active: bool
    totalAmount: int


@dataclass
class _GameMarket:
    credits: int
    incomingTransactions: List[_MarketTransaction]
    outgoingTransactions: List[_MarketTransaction]
    orders: Dict[str, _OwnedMarketOrder]

    def calcTransactionCost(self, amount: Union[int, float], roomName1: str, roomName2: str) -> int:
        pass

    def cancelOrder(self, orderId: str) -> int:
        pass

    def changeOrderPrice(self, orderId: str, newPrice: float) -> int:
        pass

    def createOrder(self, _type: str, resourceType: str, price: float, totalAmount: int, roomName: str = None) \
            -> int:
        pass

    def deal(self, orderId: str, amount: Union[int, float], yourRoomName: str = None) -> int:
        pass

    def extendOrder(self, orderId: str, addAmount: int) -> int:
        pass

    def getAllOrders(self, _filter: Union[Dict[str, Union[int, str]], Callable[[_MarketOrder], bool]]) \
            -> List[_MarketOrder]:
        pass

    def getHistory(self, resourceType: str) -> _MarketHistory:
        pass

    def getOrderById(self, _id: str) -> _MarketOrder:
        pass

@dataclass
class Game:
    constructionSites: Dict[str, ConstructionSite]
    cpu: _GameCpu
    creeps: Dict[str, Creep]
    flags: Dict[str, Flag]
    gcl: _GameGcl
    gpl: _GameGpl
    map: _GameMap
    market: _GameMarket
    resources: Dict[str, int]    
    rooms: Dict[str, Room]
    shard: _GameShard
    spawns: Dict[str, StructureSpawn]
    structures: Dict[str, OwnedStructure]
    time: int

    @classmethod
    def getObjectById(cls, _id: str) -> Optional[RoomObject]:
        pass

    @classmethod
    def notify(cls, message: str, groupInterval: int = 0) -> None:
        pass

@dataclass
class _PathFinderResult:
    path: List[RoomPosition]
    ops: int
    cost: int
    incomplete: bool

@dataclass
class PathFinder:
    @classmethod
    def search(cls, origin: RoomPosition, goal: Union[Dict[str, Any], List[Dict[str, Any]]],
               opts: Optional[Dict[str, Any]] = None) -> _PathFinderResult:
        pass

    class CostMatrix:
        def set(self, x: int, y: int, cost: int):
            pass

        def get(self, x: int, y: int) -> int:
            pass

        def clone(self) -> 'PathFinder.CostMatrix':
            pass

        def serialize(self) -> List[int]:
            pass

        @staticmethod
        def deserialize(x: List[int]) -> 'PathFinder.CostMatrix':
            pass
