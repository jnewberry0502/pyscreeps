# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages

with open('LICENSE') as f:
    license = f.read()

setup(
    name='pyscreeps',
    version='1.0.1',
    description='pyscreeps',
    long_description='pyscreeps',
    author='Justin Newberry',
    author_email='jnewberry0502@gmail.com',
    url='https://gitlab.com/jnewberry0502/pyscreeps',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=[
        'transcrypt',
        'requests'
    ]
)